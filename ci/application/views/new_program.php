<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to LOYALTY PROGRAM!</title>
<style>
	body {
	background-image: url("http://www.pixeden.com/media/k2/galleries/165/004-subtle-light-pattern-background-texture-vol5.jpg");
}

h1 {
    color: blue;
    text-align: center;
    margin-left: 40px;
	font-style: oblique;
	border-style: solid;
    border-width: medium;
}

p {
    font-family: "Times New Roman";
    font-size: 26px;
}

input[type=text],input[type=password] {
    width: 30%;
    box-sizing: border-box;
    border: 2px solid #ccc;
    border-radius: 4px;
    font-size: 16px;
    background-color: white;
    background-position: 10px 10px; 
    background-repeat: no-repeat;
    padding: 12px 20px 12px 40px;
}
input[type=text],input[type=password]:focus {
    border: 3px solid #555;
}
input[type=button], input[type=submit], input[type=reset] {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 16px 32px;
    text-decoration: none;
    margin: 4px 2px;
    cursor: pointer;
}
    .container {
        width: 500px;
        clear: both;
    }
    .container input {
        width: 100%;
        clear: both;
    }
</style>
</head>
<body>

<div id="container">
	<h1>Welcome to LOYALTY PROGRAM!</h1>

	<h2>PROGRAM SPECIFICATIONS</h2>

	<?php
		
		$this->load->helper('form');

		
		
		echo validation_errors();
		echo "<div>";
		echo   form_open('index.php/main_ctrl/authenticate_login');
		echo "<p> PROGRAM NAME";
		echo "-";
		echo " ";
		echo   form_input('prg_name','');
		echo "</p>";
		echo "<p> DESCRIPTION";
		echo "-";
		echo " ";
		echo   form_input('description','');
		echo "</p>";
		echo "</div>";

		echo "This description will be visible to the customer while registering for the program and on the card.";
	?>

		<h2>CUSTOMIZE YOUR CARD</h2>

	<?php
		
		$options = array(
                  'basic'  => 'Basic', 'red' => 'Red'
                 
                );

		echo "<p> THEME";
		echo "-";
		echo " ";
		echo form_dropdown('theme', $options, 'basic');
		echo "</p>";

		echo   form_submit('start','REGISTER');
		echo "Once registered, you can manage your program at the PROGRAM CENTRE.";

		
		
		
	?>

	
</div>

</body>
</html>

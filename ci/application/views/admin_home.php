<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome <?php  echo $this->session->userdata('username');  ?>!</title>
<style>
	body {
	background-image: url("http://www.pixeden.com/media/k2/galleries/165/004-subtle-light-pattern-background-texture-vol5.jpg");
}

h1 {
    color: blue;
    text-align: left;
	text-decoration: underline;
	text-transform: capitalize;
    margin-left: 40px;
	font-style: oblique;
}

p {
    font-family: "Times New Roman";
    font-size: 26px;
	text-transform: capitalize;
}

div.aaa {
	margin: 100px 150px 100px 80px;
}
div.bbb {
	margin: 100px 150px 100px 80px;
}
.log:link, .log:visited {
	text-shadow: 1px 1px #57D6C7;
	border-radius: 35px;
	font-size: 18px !important;
	border: 1px solid #57D6C7;
	cursor: pointer;
	box-shadow: 0 1px 0 rgba(255, 255, 255, 0.5) inset;
    background-color: #f44336;
    color: white;
    padding: 13px 25px;
    text-align: center;
    text-decoration: none;
    margin-left:40px;
    margin-top: 20px;
    display: inline-block;
}
.viewall:link, .viewall:visited {
	text-shadow: 1px 1px #57D6C7;
	border-radius: 35px;
	font-size: 18px !important;
	background-color: #57d6c7;
	border: 1px solid #57D6C7;
	cursor: pointer;
	box-shadow: 0 1px 0 rgba(255, 255, 255, 0.5) inset;
    color: black;
    padding: 13px 25px;
    text-align: center;
    text-decoration: none;
    margin-left:40px;
    margin-top: 20px;
    display: inline-block;
}
input[type=text],input[type=password] {
    width: 30%;
    box-sizing: border-box;
    border: 2px solid #ccc;
    border-radius: 4px;
    font-size: 16px;
    background-color: white;
    background-position: 10px 10px; 
    background-repeat: no-repeat;
    padding: 12px 20px 12px 40px;
}
input[type=text],input[type=password]:focus {
    border: 3px solid #555;
}
input[type=button], input[type=submit], input[type=reset] {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 16px 32px;
    text-decoration: none;
    margin: 4px 2px;
    margin-left: 40px;
    cursor: pointer;
}
input[type=checkbox] {
	margin-left: 0px;
}
</style>
	
</head>
<body>

<div id="container">
	<h1>Register a new user :-</h1>

	<?php
		
		$this->load->helper('form');
		
		echo validation_errors();
		
		echo   form_open('index.php/admin_ctrl/add_user');
		
		echo "<p> FIRST NAME -";
		echo   form_input('first_name','');
		echo "</p>";
		echo "<p> LAST NAME -";
		echo   form_input('last_name','');
		echo "</p>";
		echo "<p> EMAIL -";
		echo   form_input('email','');
		echo "</p>";
		echo "<p> USERNAME -";
		
		echo   form_input('username','');
		echo "</p>";
		echo "<p> PASSWORD -";
		echo   form_password('password','');
		echo "</p>";
		
		echo   form_submit('login','REGISTER');
		
		
		
	?>

	<?php
	
		echo  " <div><a class='viewall' href=http://localhost/ci/index.php/admin_ctrl/view_users>VIEW ALL USERS</a></div> ";
		
	?>

	<?php
	
		echo  " <div><a class='log' href=http://localhost/ci/index.php/main_ctrl/logged_out>LOGOUT</a></div> ";
		
	?>
	
	

	
</div>

</body>
</html>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');


	class Customer_model extends CI_Model {
		
	public	function  get_entries($email)
    {
        $query = $this->db->get_where('customers', array(
														'email' => $email
														 ));

		return $query->result();
    }
	
	public	function  is_valid($email,$password)
    {
        $query = $this->db->get_where('customer_unique', array(
														'email' => $email ,'password' => $password 
														 ));

		if ($query->num_rows()==1){
			
			return true ;
			
		} else return false;
    }
	
	public	function  get_all()
    {
        $query = $this->db->get('customers');

		return $query->result();
    }
		
	public function get_prgm_details($prg_id){
		
		$query = $this->db->get_where('programs', array(
														'id' => $prg_id
														 ));
														
		
		return $query->result();
	}	
		
		
	} 
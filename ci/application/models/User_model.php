<?php
defined('BASEPATH') OR exit('No direct script access allowed');


	class User_model extends CI_Model {
		
	public	function verify() // verifies valid login
    {
        $query = $this->db->get_where('users', array(
						'username' => $this->input->post('username'),
						'password' => $this->input->post('password') ));

		if ($query->num_rows()==1){
			
			return true ;
			
		} else return false;
    }
		
	public function user_type(){
		
		$query = $this->db->get_where('users', array(
						'username' => $this->input->post('username'),
						'password' => $this->input->post('password') ));
														
		$row = $query->row();
		$type = $row->type;
		return $type;
	}	

	public function user_id(){
		
		$query = $this->db->get_where('users', array(
						'username' => $this->input->post('username'),
						'password' => $this->input->post('password') ));
														
		$row = $query->row();
		$id = $row->id;
		return $id;
	}	

	public function is_team_defined(){
		
		$query = $this->db->get_where('user_type', array(
						'parent_user' => $this->session->userdata('id') ));
														
		if($query->num_rows() > 0)
		return true;  
		else 
		return false;
	}	

	public function get_team_type(){
		
		$query = $this->db->get_where('user_type', array(
						'parent_user' => $this->session->userdata('id') ));
														
		$row = $query->row();
		$type = $row->id;
		return $type;
	}

	public	function add($type)
    	{
        	$data = array(
        		'username' => $this->input->post('username'),
        		'password' => $this->input->post('password'),
			'email' => $this->input->post('email'),
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'type' => $type
		);

		$this->db->insert('users', $data);
   	}	

	public	function new_user_type($powers)
    	{
        	$data = array(
        		'parent_user' => $this->session->userdata('id'),
        		'methods_allowed' => $powers
		);

		$this->db->insert('user_type', $data);
   	}

	public	function update_powers($powers)
    	{
        	$data = array(
			'id'=> $this->get_team_type(),
        		'parent_user' => $this->session->userdata('id'),
        		'methods_allowed' => $powers
		);

		$this->db->replace('user_type', $data);
   	}		

	
		
		
	} 

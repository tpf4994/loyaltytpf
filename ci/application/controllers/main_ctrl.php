<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_ctrl extends CI_Controller {

	
	
	
	public function index()
	{
		$this->load->view('login');
	}
	
	// authenticates the login form
	public function authenticate_login (){
		
		 $this->load->library('form_validation');
		 $this->load->model('user_model');  // the model class to check in the database
		
		$this->form_validation->set_rules('username','USERNAME','required');
		$this->form_validation->set_rules('password','PASSWORD','required');
		
		if ($this->form_validation->run()){
			
			if ($this->user_model->verify()){
				//set session variables if verified 
				$userdata = array (
					'username' => $this->input->post('username'),
					'password' => $this->input->post('password'),
					'logged_in' => true,
				    'type' => $this->user_model->user_type()
				);
				$this->session->set_userdata($userdata);
				
				//$this->admin_homepage();
				
				// redirect accordingly on successful login
				if($this->user_model->user_type()=="admin") 
					$this->admin_homepage();
				else 
					$this->user_homepage(); 
				
				
				
			} else  $this->on_login_fail();
		} else $this->load->view('login'); 
		
	}
	
	
	public function admin_homepage(){
		
		
		if ($this->session->userdata('logged_in')==1&&$this->session->userdata('type')=="admin"){
			$this->load->view('admin_home');
		}
		
		else echo "you are not authorized to access this page";
		
		
	}
	
	public function user_homepage(){
		//have to load the homepage view for the reseller
		
		if ($this->session->userdata('logged_in')==1&&$this->session->userdata('type')=="reseller"){
			$this->load->view('user_home');
		}
		
		else echo "you are not authorized to access this page";
		
		
		
	}
	
	public function logged_out(){
		
		//$array_items = array('username' => '', 'password' => '','logged_in'=>'','type'=>'');

		//$this->session->unset_userdata($array_items);
		
		//log out the user by destroying the session
		
		session_destroy();
		
		
		
		$this->load->view('login');
		
	}
	
	public function on_login_fail(){
		
		
		$this->load->view('login');
		echo "   INCORRECT USERNAME OR PASSWORD";
		
		
	}
	
}

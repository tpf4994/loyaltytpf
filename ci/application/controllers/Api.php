<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'/libraries/REST_Controller.php');


class API extends REST_Controller{


	 public   function user_and_program_get()   // gives the user entries for different programs and details for those programs
    {
        $this->load->model('customer_model');
		
		$email1 = $this->uri->segment(3);
		$email2 = $this->uri->segment(4);
		$password= $this->uri->segment(5);
		
		
		$email = $email1 +"@"+$email2 +".com";
		
		$cust_verify = $this->customer_model->is_valid($email,$password);
		
		if ($cust_verify){
			
			$cust_entries = $this->customer_model->get_entries($email);
			
			$program_details ;
		
				foreach ($cust_entries as $row)
			{
				$prg_id = $row->program_id;
				$program_details[]= $this->customer_model->get_prgm_details($prg_id);
			}
			
			
			$this->response(array('success'=>true,'entries'=>$cust_entries,'programs'=>$program_details));
		}
		
		else {
			$this->response(array('success' => false));
		}
		
		
    }
} 

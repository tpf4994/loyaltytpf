<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This class will have all the features available to the reseller in the form of methods 
// Each method will have its own id 
// This id of each method will help in establishing heirarchy of users
// How ? - every user in the heirarchy will have the feature of registering a new person/persons under him/her 
// Before doing that , he will have to specify the features that he wishes to expose to them using the method id's
// These features(those assigned to the new type of workforce) will have to be a subset of the features exposed to him/her 

//NOTE: EVERY TIME YOU ADD A METHOD, GIVE IT AN ID  AND ALSO UPDATE THE STRUCTURE OF THE USER_TYPE TABLE COLUMN 'methods_allowed'  
//ALSO UPDATE THE UPDATE TEAM POWERS METHOD IN HERE WITH AND ALSO THE MANAGE TEAM VIEW FILE
class User_ctrl extends CI_Controller{

////////	/*********** REMINDER : APPLYING A CHECK WHETHER A PARTICULAR USER IS AUTHORISED TO USE A PARTICULAR METHOD
	//            STILL REMAINS. DELETE THIS COMMENT AFTER APPLYING THE CHECK**********************//////////////////

	// METHOD ID 1  - create a new program
	public function new_program(){

		if ($this->session->userdata('logged_in')==1){

			$this->load->view('new_program');
			
		}		

		else echo "please login for access";


	} 

	
	// METHOD ID 2 - managing his/her own team
	public function manage_team(){

		$this->load->view('manage_team');

	}

	// METHOD ID 3 - adding a new member to the team
	public function add_member(){

		$this->load->model('user_model'); // loading the db model 

		if ($this->user_model->is_team_defined())  // go ahead only if the user has specified the team parameters

		{
			$type = $this->user_model->get_team_type();
			
			if ($this->session->userdata('logged_in')==1) // verifying valid login
			
			{
				$this->load->library('form_validation');
				$this->load->model('admin_model');  
		
				$this->form_validation->set_rules('username','USERNAME','required|is_unique[users.username]');
				$this->form_validation->set_rules('password','PASSWORD','required');
				$this->form_validation->set_rules('first_name','FIRST NAME','required');
				$this->form_validation->set_rules('email','EMAIL','required|valid_email|is_unique[users.email]');
				$this->form_validation->set_rules('last_name','LAST NAME','required');
		
				if ($this->form_validation->run()){
		
					$this->user_model->add($type);
					echo "New member added";
					$this->load->view('manage_team');
		
				} else $this->load->view('manage_team');
		
			}
			else echo "Unauthorized access";

		} else {
				echo "You have to specify the team powers before adding a new member";
			 	$this->load->view('manage_team');	
			}


	
		

		}
	

	// METHOD ID 4 - updating the powers of the team
	public function update_team_powers(){
		
		$this->load->model('user_model'); 
		$powers ="";
		$test="1,2,3";

		$pow1 = $this->input->post('powers1');		
		if (!empty($pow1))
		{
			$powers= "1" ;
		}

		$pow2 = $this->input->post('powers2');		
		if (!empty($pow2))
		{
			if ($powers=="")$powers="2";
			else				
			$powers= $powers.",2" ;
		}

		if ($this->user_model->is_team_defined()){
			
			$this->user_model->update_powers($powers);
			$this->load->view('manage_team');
			echo "Powers updated successfully !";
			
		}
		else
		{
			$this->user_model->new_user_type($powers);
			$this->load->view('manage_team');
			echo "Team powers initiated successfully!";
			
		}

	}











} 

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<style>
	body {
	background-image: url("http://www.pixeden.com/media/k2/galleries/165/004-subtle-light-pattern-background-texture-vol5.jpg");
}

h1 {
    color: blue;
    text-align: left;
	text-decoration: underline;
	text-transform: capitalize;
    margin-left: 40px;
	font-style: oblique;
}

p {
    font-family: "Times New Roman";
    font-size: 26px;
	text-transform: capitalize;
}

p.useruser {
    font-family: "Times New Roman";
    font-size: 26px;
	text-transform: capitalize;
	border-style: solid;
    border-width: medium;
}

div.aaa {
	margin: 100px 150px 100px 80px;
}
div.bbb {
	margin: 100px 150px 100px 80px;
}
.goback:link, .goback:visited {
	text-shadow: 1px 1px #57D6C7;
	border-radius: 35px;
	font-size: 18px !important;
	border: 1px solid #57D6C7;
	cursor: pointer;
	box-shadow: 0 1px 0 rgba(255, 255, 255, 0.5) inset;
    background-color: #f44336;
    color: white;
    padding: 13px 25px;
    text-align: center;
    text-decoration: none;
	float:right;
    display: inline-block;
}
</style>
	
</head>
<?php
class Admin_ctrl extends CI_Controller {
	
	
	public function admin_home()
	{
		$this->load->view('admin_home');
	}
	public function add_user (){
		
		if ($this->session->userdata('logged_in')==1&&$this->session->userdata('type')=="admin")
		{
		$this->load->library('form_validation');
		$this->load->model('admin_model');  
		
		$this->form_validation->set_rules('username','USERNAME','required|is_unique[users.username]');
		$this->form_validation->set_rules('password','PASSWORD','required');
		$this->form_validation->set_rules('first_name','FIRST NAME','required');
		$this->form_validation->set_rules('email','EMAIL','required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('last_name','LAST NAME','required');
		
		if ($this->form_validation->run()){
		
			$this->admin_model->add();
			echo "new user added";
			$this->load->view('admin_home');
		
		} else $this->load->view('admin_home');
		
		}
		else echo "unauthorized access";
		
	}
	
	public function view_users (){
		
		$this->load->model('admin_model');  
		$query = $this->admin_model->get_users();
		$count=1;
		echo  " <div><a class='goback' href=http://localhost/ci/index.php/admin_ctrl/admin_home>GO BACK</a></div> ";
	echo "<h1>ALL USERS : </h1>";
		foreach ($query->result()as $users)
	{
		echo "<p >";
		echo "$count";
		echo ".) ";
        echo "$users->first_name";
		
		echo " ";
        echo "$users->last_name";
		echo " ";
		echo " -{";
		echo " ";
        echo "$users->date_joined";
		echo " ";
		echo "}</p>";
		$count=$count+1;
	}
		
		//$this->load->view('admin_home');
	}
	
	
	
}
?>
</html>